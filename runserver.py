import uvicorn

from core import create_app

app = create_app()

if __name__ == '__main__':
    uvicorn.run(
        app='runserver:app',
        reload=True,
        port=8001,
        host='localhost',
    )
