import asyncio
from pathlib import Path

from aiohttp import ClientSession
from typer import Typer, Argument

FER_URL = 'http://localhost:8001'
IMAGE_URL = f'{FER_URL}/image'
VIDEO_URL = f'{FER_URL}/video'

app = Typer()


async def file_process(url: str, file_name: str, res: Path, dest: Path):
    async with ClientSession() as session:
        with res.open('rb') as file:
            resp = await session.post(url, data={file_name: file})

        with dest.open('wb') as file:
            file.write(await resp.read())


@app.command('image')
def image_process(
    res: Path = Argument(..., file_okay=True),
    dest: Path = Argument(...),
):
    asyncio.run(file_process(IMAGE_URL, 'image', res, dest))


@app.command('video')
def image_process(
    res: Path = Argument(..., file_okay=True),
    dest: Path = Argument(...),
):
    asyncio.run(file_process(VIDEO_URL, 'video', res, dest))


@app.command('webcam')
def image_process(
    res: Path = Argument(..., file_okay=True),
    dest: Path = Argument(...),
):
    print('Hi')


if __name__ == '__main__':
    app()
