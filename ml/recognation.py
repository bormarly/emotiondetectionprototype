from pathlib import Path

import cv2
import numpy as np
from keras.preprocessing import image
from keras.models import Sequential, load_model

FACE_CLASSIFIER_FILE = Path('../trained_models/haarcascade_frontalface_default.xml')
EMOTION_CLASSIFIER_FILE = Path('../trained_models/custom.h5')

face_classifier = cv2.CascadeClassifier(str(FACE_CLASSIFIER_FILE))
emotion_classifier: Sequential = load_model(EMOTION_CLASSIFIER_FILE)

class_labels = [
    'angry',
    'disgust',
    'fear',
    'happy',
    'neutral',
    'sad',
    'surprise',
]


cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1080)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)


def main():
    while True:
        ret, frame = cap.read()
        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = frame
        faces = face_classifier.detectMultiScale(gray, 1.3, 5)

        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_gray: np.ndarray = cv2.resize(roi_gray, (48, 48), interpolation=cv2.INTER_AREA)

            if np.sum([roi_gray]) != 0:
                roi = roi_gray.astype(np.float) / 255.0
                roi = image.img_to_array(roi)
                roi = np.expand_dims(roi, axis=0)
                preds = emotion_classifier.predict(roi)[0]
                label = class_labels[preds.argmax()]
                label_position = (x, y)
                # print(preds)
                cv2.putText(frame, label, label_position, cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)
            else:
                cv2.putText(frame, 'No face found', (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)

        cv2.imshow('Emotion Detector', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


try:
    main()
finally:
    cap.release()
    cv2.destroyAllWindows()
