import matplotlib.pyplot as plt
from keras import optimizers, callbacks
from keras_preprocessing.image import ImageDataGenerator


from ml_models import get_model
from datasets import FER2013_DIR
from trained_models import MODELS_DIR
from config import IMG_ROWS, IMG_COLS, BATCH_SIZE

TRAIN_DIR = FER2013_DIR / 'train'
VALIDATION_DIR = FER2013_DIR / 'val'

train_datagen = ImageDataGenerator(
    rescale=1/255,
    rotation_range=30,
    shear_range=0.3,
    zoom_range=0.3,
    width_shift_range=0.4,
    height_shift_range=0.4,
    horizontal_flip=True,
    fill_mode='nearest',
)

validation_datagen = ImageDataGenerator(rescale=1/255)

train_generator = train_datagen.flow_from_directory(
    directory=TRAIN_DIR,
    color_mode='grayscale',
    target_size=(IMG_ROWS, IMG_COLS),
    batch_size=BATCH_SIZE,
    class_mode='categorical',
    shuffle=True,
)

validation_generator = validation_datagen.flow_from_directory(
    directory=VALIDATION_DIR,
    color_mode='grayscale',
    target_size=(IMG_ROWS, IMG_COLS),
    batch_size=BATCH_SIZE,
    class_mode='categorical',
    shuffle=True,
)

checkpoint = callbacks.ModelCheckpoint(
    str(MODELS_DIR.joinpath('EmotionDetectionModel_V2.h5')),
    monitor='val_loss',
    mode='min',
    save_best_only=True,
    verbose=1,
)

earlystop = callbacks.EarlyStopping(
    monitor='val_loss',
    min_delta=0,
    patience=3,
    verbose=1,
    restore_best_weights=True,
)

reduce_lr = callbacks.ReduceLROnPlateau(
    monitor='val_loss',
    factor=0.2,
    patience=3,
    verbose=1,
    min_delta=0.0001,
)

model_callbacks = [
    earlystop,
    checkpoint,
    reduce_lr,
]


model = get_model()


model.compile(
    loss='categorical_crossentropy',
    optimizer=optimizers.Adam(lr=0.001),
    metrics=['acc'],
)


NB_TRAIN_SAMPLES = 24176
NB_VALIDATION_SAMPLES = 3006
EPOCHS = 25

history = model.fit(
    train_generator,
    steps_per_epoch=NB_TRAIN_SAMPLES // BATCH_SIZE,
    epochs=EPOCHS,
    callbacks=model_callbacks,
    validation_data=validation_generator,
    validation_steps=NB_VALIDATION_SAMPLES // BATCH_SIZE,
)

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation acc')

plt.figure()

plt.plot(epochs, loss, 'bo', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')

plt.legend()

plt.show()
