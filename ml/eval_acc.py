from keras import models
from keras_preprocessing.image import ImageDataGenerator

from datasets import TEST_DIR
from trained_models import MODELS_DIR

BATCH_SIZE = 32

test_datagen = ImageDataGenerator(rescale=1/255)
test_gen = test_datagen.flow_from_directory(
    TEST_DIR,
    target_size=(48, 48),
    batch_size=BATCH_SIZE,
)

model: models.Sequential = models.load_model(
    MODELS_DIR.joinpath('VGG16-with-custom-top.h5'),
    compile=True,
)

score, acc = model.evaluate(
    test_gen,
    batch_size=BATCH_SIZE
)

print(score)
print(acc)
