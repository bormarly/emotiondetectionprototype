from keras import layers, models

from train_settings import (
    PADDING,
    IMG_COLS,
    IMG_ROWS,
    POOL_SIZE,
    CONV_SIZE,
    ACTIVATION,
    NUM_CLASSES,
    KERNEL_INITIALIZER,
)


def get_model() -> models.Sequential:
    model = models.Sequential()

    # Block-1
    model.add(layers.Conv2D(
        32,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
        input_shape=(IMG_ROWS, IMG_COLS, 1),
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(
        32,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
        # input_shape=(IMG_ROWS, IMG_COLS, 3),
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=POOL_SIZE))
    model.add(layers.Dropout(0.2))

    # Block-2
    model.add(layers.Conv2D(
        64,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(
        64,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=POOL_SIZE))
    model.add(layers.Dropout(0.2))

    # Block-3
    model.add(layers.Conv2D(
        128,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(
        128,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=POOL_SIZE))
    model.add(layers.Dropout(0.2))

    # Block-4
    model.add(layers.Conv2D(
        256,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(
        256,
        CONV_SIZE,
        padding=PADDING,
        kernel_initializer=KERNEL_INITIALIZER,
    ))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=POOL_SIZE))
    model.add(layers.Dropout(0.2))
    # Block-5
    model.add(layers.Flatten())
    model.add(layers.Dense(64, kernel_initializer=KERNEL_INITIALIZER))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.5))

    # Block-6
    model.add(layers.Dense(64, kernel_initializer=KERNEL_INITIALIZER))
    model.add(layers.Activation(ACTIVATION))
    model.add(layers.BatchNormalization())
    model.add(layers.Dropout(0.5))

    # Block-7
    model.add(layers.Dense(NUM_CLASSES, kernel_initializer=KERNEL_INITIALIZER))
    model.add(layers.Activation('softmax'))

    print(model.summary())

    return model
