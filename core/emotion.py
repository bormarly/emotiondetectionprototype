from io import BytesIO
from typing import Optional
from tempfile import NamedTemporaryFile


import cv2
import numpy as np
from tqdm import tqdm
import pybase64 as base64
from keras.preprocessing import image

from core import shared
from core.enums import Emotion
from core import transformations as t
from config import IMG_ROWS, IMG_COLS
from datasets import BUFFER_WRITE_FILE
from core.models import (
    PredictionBase64,
    Prediction,
    EmotionPrediction,
    EmotionsState,
    PredictionBytes,
)


def detect_emotions_from_base64(img_base64: str) -> Optional[PredictionBase64]:
    img_bytes = t.img_base642bytes(img_base64)

    if not img_bytes:
        return None

    pred = detect_emotions_from_bytes(img_bytes)
    pred = PredictionBase64(
        image=t.img_bytes2base64(pred.image),
        emotions=pred.emotions,
    )
    return pred


def detect_emotions_from_bytes(img_bytes: bytes) -> PredictionBytes:
    img_arr = t.img_bytes2arr(img_bytes)
    res = detect_emotions(img_arr)
    img_bytes = t.img_arr2bytes(res.image)
    return PredictionBytes(
        image=img_bytes,
        emotions=res.emotions,
    )


def detect_emotions_from_video(video: bytes) -> BytesIO:
    with NamedTemporaryFile('wb+', suffix='.mp4') as bfile:

        bfile.write(video)
        bfile.seek(0)

        cap = cv2.VideoCapture(bfile.name)
        writer = cv2.VideoWriter(
            str(BUFFER_WRITE_FILE),
            cv2.VideoWriter_fourcc(*'mp4v'),
            cap.get(cv2.CAP_PROP_FPS),
            (
                int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),
            ),
            True,
        )

        bar = tqdm(desc='Process Video', total=int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))
        while cap.isOpened():
            ret, frame = cap.read()  # type: np.ndarray
            if not ret:
                break
            resp = detect_emotions(frame)
            writer.write(resp.image)
            bar.update()

        cap.release()
        writer.release()
        bar.close()

    buffer = BytesIO()
    buffer.write(BUFFER_WRITE_FILE.read_bytes())
    buffer.seek(0)

    return buffer


def detect_emotions(img_arr: np.ndarray) -> Prediction:

    gray = cv2.cvtColor(img_arr, cv2.COLOR_BGRA2BGR)

    faces_coords = shared.face_classifier.detectMultiScale(gray, 1.3, 5)

    face_preds = []

    for face_idx, (x, y, w, h) in enumerate(faces_coords):
        cv2.rectangle(img_arr, (x, y), (x + w, y + h), (255, 0, 0), 2)
        face = gray[y:y + h, x:x + w]
        face: np.ndarray = cv2.resize(face, (IMG_ROWS, IMG_COLS), interpolation=cv2.INTER_AREA)

        if len(face) != 0:
            face = face.astype(np.float) / 255
            face = image.img_to_array(face)
            face = np.expand_dims(face, axis=0)
            pred: np.ndarray = shared.emotion_classifier.predict(face)[0]
            max_pos: int = pred.argmax()
            emotion = Emotion.values()[max_pos]
            emotion_pos = (x, y - 10)
            cv2.putText(
                img_arr,
                f'{emotion} - {face_idx}',
                emotion_pos,
                cv2.FONT_HERSHEY_SIMPLEX,
                3,
                (0, 255, 0),
                3,
            )
            face_preds.append(EmotionPrediction(
                face_idx=face_idx,
                emotions_state=EmotionsState(**dict(zip(Emotion.values(), pred)))
            ))

    return Prediction(
        image=img_arr,
        emotions=face_preds,
    )
