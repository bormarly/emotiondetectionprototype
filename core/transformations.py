from typing import Callable

import cv2
import numpy as np
import pybase64 as base64

def img_bytes2arr(img: bytes) -> np.ndarray:
    return cv2.imdecode(
        np.frombuffer(img, np.uint8),
        cv2.IMREAD_COLOR,
    )


def img_arr2bytes(img: np.ndarray) -> bytes:
    _, img = cv2.imencode('.png', img)  # type: np.ndarray
    return img.tobytes()


def img_base642bytes(img: str) -> bytes:
    return base64.b64decode(img)


def img_bytes2base64(img: bytes) -> str:
    return base64.b64encode(img).decode('utf-8')


def pipe(obj, *funcs: Callable):
    for func in funcs:
        obj = func(obj)
    return obj
