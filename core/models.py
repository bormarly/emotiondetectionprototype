from typing import List, Any

from pydantic import BaseModel, validator


class EmotionsState(BaseModel):
    angry: float = 0
    disgust: float = 0
    fear: float = 0
    happy: float = 0
    neutral: float = 0
    sad: float = 0
    surprise: float = 0

    @validator('angry', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise')
    def _round(cls, value: float) -> float:
        return round(value * 100, 2)


class EmotionPrediction(BaseModel):
    emotions_state: EmotionsState
    face_idx: int


class Prediction(BaseModel):
    image: Any
    emotions: List[EmotionPrediction]


class PredictionBase64(Prediction):
    image: str


class PredictionBytes(Prediction):
    image: bytes
