from functools import lru_cache

import cv2 as cv
from keras import Sequential, models

from trained_models import get_model_path, HAAR_CASCADE


@lru_cache()
def get_emotion_classifier() -> Sequential:
    model_path = get_model_path('custom.h5')
    return models.load_model(model_path)


@lru_cache()
def get_face_classifier() -> cv.CascadeClassifier:
    return cv.CascadeClassifier(str(HAAR_CASCADE))


emotion_classifier = get_emotion_classifier()
face_classifier = get_face_classifier()
