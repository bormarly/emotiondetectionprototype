from enum import Enum
from typing import List


class StrEnum(str, Enum):

    @classmethod
    def values(cls) -> List[str]:
        return list(map(str, cls))

    def __repr__(self):
        return self.value

    def __str__(self):
        return self.value


class Emotion(StrEnum):
    ANGRY = 'angry'
    DISGUST = 'disgust'
    FEAR = 'fear'
    HAPPY = 'happy'
    NEUTRAL = 'neutral'
    SAD = 'sad'
    SURPRISE = 'surprise'
