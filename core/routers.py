from io import BytesIO

from fastapi.responses import StreamingResponse
from fastapi import APIRouter, WebSocket, File, UploadFile

from core import emotion

router = APIRouter()


@router.get('/', tags=['Base'])
async def root():
    return {
        'message': 'Hello Word',
    }


@router.post('/image', tags=['Image'], response_class=StreamingResponse)
async def process_image(image: UploadFile = File(...)) -> StreamingResponse:
    prediction = emotion.detect_emotions_from_bytes(await image.read())
    buffer = BytesIO()
    buffer.write(prediction.image)
    buffer.seek(0)

    headers = {
        'Emotions': prediction.json(exclude={'image'}),
    }

    return StreamingResponse(buffer, media_type="image/png", headers=headers)


@router.post('/video', tags=['Video'], response_class=StreamingResponse)
async def process_video(video: UploadFile = File(...)) -> StreamingResponse:
    buffer = emotion.detect_emotions_from_video(await video.read())
    return StreamingResponse(buffer, media_type="video/mp4")


@router.websocket('/ws')
async def emotion_detect(ws: WebSocket):
    await ws.accept()

    while True:
        data = await ws.receive_text()
        *_, img_base64 = data.split(',')
        pred = emotion.detect_emotions_from_base64(img_base64)
        if pred:
            await ws.send_json(pred.dict())
