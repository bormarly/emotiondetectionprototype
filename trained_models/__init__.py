from pathlib import Path

MODELS_DIR = Path(__file__).parent
HAAR_CASCADE = MODELS_DIR / 'haarcascade_frontalface_default.xml'


def get_model_path(model_name: str) -> Path:
    path = MODELS_DIR / model_name
    if not path.exists():
        raise FileNotFoundError(f'Model {model_name!r} not found in {MODELS_DIR}')
    return path
