from pathlib import Path

DATASET_DIR = Path(__file__).parent

# FER2013_DIR = DATASET_DIR / 'fer2013'
FER2013_DIR = Path('D:/fer2013/data')

TRAIN_DIR = FER2013_DIR / 'train'
VAL_DIR = FER2013_DIR / 'val'
TEST_DIR = FER2013_DIR / 'test'


BUFFER_WRITE_FILE = Path('/home/volodymyrb/tmp-video.mp4')
